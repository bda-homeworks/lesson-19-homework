import java.io.*;

public class CustomFileWriter {
    void yazmaString () {
        try {
            FileWriter yazma = new FileWriter("fileWriterString.txt");
            yazma.write("Salam \n Privet");
            yazma.append("Sagol");
            yazma.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void yazmaByte () {
        try {
            FileOutputStream yazma = new FileOutputStream("fileWriterByte.txt");
            String data = "Salam olsun byte-lardan";
            byte[] arr = data.getBytes();
            yazma.write(arr);
            yazma.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void yazmaObject (Student obyekt) {
        try {
            File file = new File("fileWriterObject.txt");
            ObjectOutputStream yazma = new ObjectOutputStream(new FileOutputStream(file));
            yazma.writeObject(obyekt);
            yazma.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
