public class Main {
    public static void main(String[] args) {
        CustomFileReader oxumaq = new CustomFileReader();
        CustomFileWriter yazmaq = new CustomFileWriter();
        Student telebe = new Student("Elmin", "Mugalov", 21);

        yazmaq.yazmaString();
        yazmaq.yazmaByte();
        yazmaq.yazmaObject(telebe);

        oxumaq.oxumaString();
        oxumaq.oxumaByte();

    }
}
