import java.io.*;

public class CustomFileReader {
    void oxumaString () {
        File file = new File("fileWriterString.txt");
        Reader oxu = null;
        try {
            oxu = new FileReader(file);
            char c[] = new char[(int) file.length()];
            oxu.read(c);
            String data = new String(c);
            System.out.println("Fayldaki yazi => " + data);
            oxu.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void oxumaByte () {
        try {
            File file = new File("fileWriterByte.txt");
            InputStream read = new FileInputStream("fileWriterByte.txt");
            read.read();
            read.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
