import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        File dictionary = new File("Words.txt");
        Scanner scanner = new Scanner(System.in);
        boolean continueLoop = true;
        while (continueLoop) {
            System.out.println("1. Azerbaijan to English.\n2. English to Azerbaijan. \n3. Show wordlist. \n4. Show words count \n5. Add new words. \n6. Delete last added word.\n7. Exit.");
            System.out.println("--------------------------------------------------------------");
            int choice = scanner.nextInt();
            switch (choice) {
                case 1:
                    translateAze();
                    break;
                case 2:
                    translateEng();
                    break;
                case 3:
                    printWords();
                    break;
                case 4:
                    countLine(dictionary);
                    break;
                case 5:
                    addWords();
                    break;
                case 6:
                    deleteLastLine();
                    break;
                case 7:
                    System.exit(0);
                    break;
                default:
                    System.out.println("No such choice.....");
                    break;
            }
            if (choice == 6) {
                continueLoop = true;
            }
        }
        System.out.println("Leaving the system.");
    }

    public static long countLine (File fileName) {
        long lines = 0;
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            while (reader.readLine() != null) lines++;
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("There are: " + lines + " words.");
        System.out.println("--------------------------------------------------------------");
        return lines;
    }
    public static void addWords () {
        Scanner scanner = new Scanner(System.in);
        System.out.println("--------------------------------------------------------------");
        System.out.println("Adding new word:");
        System.out.print("In Azerbaijan => ");
        String word1 = scanner.next();
        System.out.print("In English => ");
        String word2 = scanner.next();
        String word3 = word1 + "&" + word2;
        System.out.println("Word succesfully added in list.");
        System.out.println("--------------------------------------------------------------");
        try {
            BufferedWriter yazma = new BufferedWriter(new FileWriter("Words.txt", true));
            yazma.write(word3);
            yazma.newLine();
            yazma.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void printWords () {
        try {
            BufferedReader reader = new BufferedReader(new FileReader("Words.txt"));
            String line;
            System.out.println("Azerbaijan~ - English~");
            while ((line = reader.readLine()) != null) {
                String line2 = line.replace("&", " --------- ");
                System.out.println(line2);
            }
            reader.close();
            System.out.println("--------------------------------------------------------------");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void translateAze() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the word you want to translate from Azerbaijan to English: ");
        String word = scanner.next();
        try {
            BufferedReader freader = new BufferedReader(new FileReader("Words.txt"));
            String s;
            while ((s = freader.readLine()) != null) {
                String[] st = s.split("&");
                String aze = st[0];
                String eng = st[1];
                if (aze.equalsIgnoreCase(word)){
                    System.out.println("in Azerbiajan: " + aze + " in English is => " + eng);
                    System.out.println("--------------------------------------------------------------");
                }
            }
            freader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void translateEng() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the word you want to translate from English to Azerbaijan: ");
        String word = scanner.next();
        try {
            BufferedReader freader = new BufferedReader(new FileReader("Words.txt"));
            String s;
            while ((s = freader.readLine()) != null) {
                String[] st = s.split("&");
                String eng = st[1];
                String aze = st[0];
                if (word.equalsIgnoreCase(eng)) {
                    System.out.println("in English: " + eng + " in Azerbaijan is => " + aze);
                    System.out.println("--------------------------------------------------------------");
                }
            }
            freader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void deleteLastLine () throws IOException {
        RandomAccessFile f = new RandomAccessFile("Words.txt", "rw");
        long length = f.length() - 1;
        byte b;
        do {
            length -= 1;
            f.seek(length);
            b = f.readByte();
        } while (b != 10);
        f.setLength(length + 1);
        f.close();
    }
}
